import Customer from '../models/Customer'
import * as service from '../services/customers'

export default function customersController (api) {
  api.get('/customers', (req, res, next) => {
    return service.list(req.query, req.currentUser).then(page => res.json(page))
  })
  api.post('/customers', (req, res, next) => {
    const customer = {
      ...req.body.customer,
      user_id: req.currentUser.id
    }
    return Customer.forge(customer).save().then(
      customer => res.json({ customer })
    ).catch(next)
    // .catch(() => res.status(422).end())

    // Note: we do not handle the exception here, we pass it to the next handler.
    // See server.js, where the apiRoutes (express.Router) handles ValidationException.
  })
}
