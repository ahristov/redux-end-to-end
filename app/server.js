import express from 'express'
import morgan from 'morgan'
import cors from 'cors'
import cookieParser from 'cookie-parser'
import bodyParser from 'body-parser'
import { decode, apiRoutes } from './server/authentication'
import render from './serverRender'
import customersController from './server/controllers/customers'
import invoicesController from './server/controllers/invoices'
import ValidationException from './server/models/ValidationException'
/*

import User from './server/models/User'
import { kx } from './server/connection'

// Knex & Bookshelf examples

function lookup(username = '') {
    return User.where({ username }).fetch().then(user => user && user.toJSON())
}

kx.on('query', data => console.log(data))

const invoices = kx.from('invoices')
    .select('total', 'email', 'username')
    .innerJoin('users', 'users.id', 'invoices.user_id')
    .then(rows => {
        return rows
    })

const users = kx.from('users')
    .select('username', 'total', 'email')
    .innerJoin('invoices', 'invoices.user_id', 'users.id')
    .then(rows => {
        return rows
    })

const eagerUsers = User.fetchAll({
    withRelated: ['invoices']
}).then(relation => {
    debugger
    return relation
})

*/

/*
let proxyOptions = {
    target: 'http://localhost:9999',
    changeOrigin: true,
    logLevel: 'debug',
    onError: function(err, req, res) {
        console.log('Something went wrong with the proxy middleware', err)
        res.end()
    }
}
*/

const app = express()
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(cors({ origin: 'http://localhost:3000', credentials: true }))
// app.use(allowCrossDomain)
app.use(morgan('dev'))
app.use(decode)
// app.use('/api', proxy(proxyOptions), apiRoutes)
app.use('/api', apiRoutes)

customersController(apiRoutes)
invoicesController(apiRoutes)

/*
The structure of the response matches the React form data:
{
  "errors": {
    "email": "Not Valid"
   }
}
*/

apiRoutes.use((err, _, res, next) => {
  if (err instanceof ValidationException) {
    res.status(422).json({ errors: err.errors })
  } else {
    next(err)
  }
})

app.use('/assets', express.static('assets'))
app.use(render)
app.listen(9999)
