Redux End to End
===

Contains notes from [Redux – End to End](https://www.packtpub.com/mapt/video/video/9781788394277)

To run the project execute:

    yarn install
    yarn global add knex
    yarn global add babel-node
    yarn start

Side note - for VS Code I install:

  yarn global add babel-cli typescript eslint eslint-plugin-react eslint-config-standard-jsx tslint

release/1.0.0-BasicReactSetup: How to setup new project
---

Install `yarn`:

    brew install yarn

Create new project:

    mkdir redux-end-to-end
    cd redux-end-to-end
    yarn init
    yarn add --dev webpack webpack-dev-server browser-sync browser-sync-webpack-plugin

Create configuration file for Webpack `webpack.config.js`:

	var path = require('path')
	var webpack = require('webpack')
	var BrowserSyncPlugin = require('browser-sync-webpack-plugin')
    
	module.exports = {
	  entry: {
	    js: './index.js'
	  },
	  output: { path: __dirname, filename: 'bundle.js' },
	  devtool: "#cheap-module-source-map",
	  devServer: {
	    historyApiFallback: true
	  },
	  resolve: {
	    extensions: ['.js', '.jsx']
	  },
	  module: {
	    rules: [{
	      test: /.jsx?$/,
        use: ['babel-loader'],
        exclude: /node_modules/
      }, {
        test: /.scss$/,
        use: ['style-loader', 'css-loader', 'sass-loader']
      }]
    },
      plugins: [
       new BrowserSyncPlugin({
         host: 'localhost',
        port: 3000,
        proxy: 'http://localhost:8080/'
       })
      ]
    }
	


Then create `index.html` page to be served:

    <!DOCTYPE html>
    <html>
      <head>
        <meta charset="utf-8">
        <title>Redux: End to End</title>
        <link href="https://fonts.googleapis.com/css?family=Fira+Sans:300,400,700" rel="stylesheet">
        <link rel="stylesheet" href="https://unpkg.com/purecss@1.0.0/build/pure-min.css" integrity="sha384-nn4HPE8lTHyVtfCBi5yW9d20FjT8BJwUXyWZT9InLYax14RDjBj46LmSztkmNP9w" crossorigin="anonymous">
      </head>
      <body>
        <div id="app"></div>
      </body>
      <script src="/bundle.js"></script>
    </html>

Add entry `index.js` file to wire up the code:

    import './app/client.jsx'

Add simple `app\client.jsx`

    import React from 'react'
    import { render } from 'react-dom'
    
    render(
      <h1>Hello React!</h1>
      document.getElementById("app")
    )

Add `start` script to the package.json config:

    {
      "name": "redux-end-to-end",
      "version": "1.0.0",
      "main": "index.js",
      "scripts": {
        "start": "webpack-dev-server --progress --colors -w"
      },
      "license": "MIT",
      "devDependencies": {
        "browser-sync": "^2.18.13",
        "browser-sync-webpack-plugin": "^1.2.0",
        "webpack": "^3.5.6",
        "webpack-dev-server": "^2.7.1"
      }
    }

Run the app:

    yarn start

release/1.3.0-ServingReactFromExpress
---

We need to install `express` to create isomorphic app. We also need `babel-cli` in order to run the server.

    yarn add express
    yarn add --dev babel-cli

Check `app\server.js` - a minimal web server.

It loads `app\template.js` - just contains the static html.

We add __server__ script to the `package.json`:

    "scripts": {  
      "server": "yarn run build-client && babel-node app/server.js --presets es2015,react,stage-2",  
      "build-client": "webpack --config ./webpack.config.ssr.js",  
      "start": "webpack-dev-server --progress --colors -w"  
    },  


Note: _react_ preset will be needed to isomorphic rendering.

Run the server:

    yarn run server

This will just serve static html.

To serve React from the server we create second webpack config file: `webpack.config.ssr.js`.
Also, the bundle will be served from `/assets/bundle.js`, not from just `/bundle.js`.
The directory `/assets` will contain all the static files.
Note: Use `UglifyJsPlugin` to minify the assets.

Hot Reloading:

This webpack config will be _without the server sync plugin_. Instead **hot module reloading** will be used.
The config with hot module reloading module is constantly changing area.
Check the server side framework `Next.js`, which deals with the complexity of serving server-rendered React apps.

release/1.4.0-LoadingSASSModules
---

We need few more loaders:

  yarn add --dev node-sass style-loader css-loader sass-loader

Then set in the webpack.config.js:

    module: {
      rules: [{
        ...
      }, {
        test: /.scss$/,
        use: ['style-loader', 'css-loader', 'sass-loader']
      }]

This will put all the css into `<style/>` tags in the header.
This is fine for development mode, but for production we want to use the `ExtractText` webpack plugin.
This plugin takes all the scss imports and creates one css file, that we can reference from the html.

Drawback - no hot module reload works with this method. We will need to reload the page after every css change. So we can use on the server side rendering configuration.

Install ExtractText the npm package:

  yarn add --dev extract-text-webpack-plugin

Add to the webpack config for server side rendering. Import the plugin:

  var ExtractTextPlugin = require('extract-text-webpack-plugin')

Add entry to the modules:

    module: {
      ...
      }, {
        test: /.scss$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: ['css-loader', 'sass-loader']
        })

Finally add this entry to the plugins section that specifies the output file name.

  plugins: [
    ...
    new ExtractTextPlugin({
      filename: `app.css`,
      allChunks: true
    })

Then add link to the app.css in the `template.js` file:

    <link rel="stylesheet" href="assets/app.css">

We also need to add to the webpack config for server side rendering a _define plugin_, to signal React we are in production:

    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify('production')
      }
    })

release/1.5.0-IsomorphicRendering
---

We move the UI components into _app/universal_ folder. Then update the references in the `client.jsx`.

Create _middleware_ component file `serverRender.jsx`. It renders the virtual DOM into a string, that gets injected to the output.

Update the `template.js` to return a function instead of the static template. Change the first line:

    export default `<!DOCTYPE html>

to this:

    export default html => `<!DOCTYPE html>

, then use string interpolation to insert the html:

    <div id="app">${html}<div>

Lastly import the `serverRender.jsx` middleware component into `server.js` and use it. Replace thuis:

    import template from './template'

with this:

    import render from './serverRender'

, and use it:

    app.use(render)

Test:

    yarn run server

If the server side returns an html that is different than the client side html, React will replace it. For example, 
if the `serverRender.jsx` returned:

    const html = renderToString(<h2>Hello World!</h2>)

, still the client will render the `RGB.jsx` control.

React will show warnings only if we remove the production optimization and minification flags from the plugins,
so we could delete these from the `webpack.config.ssr.js`:

      new webpack.DefinePlugin({
        'process.env': {
          'NODE_ENV': JSON.stringify('production')
        }
      }),
      new webpack.LoaderOptionsPlugin({
        minimize: true,
        debug: false
      }),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false,
      ...

and test again. If we look at the browser debug console, we will see errors:

    Warning: React attempted to reuse markup in a container but the checksum was invalid. This generally means that you are using server rendering and the markup generated on the server was not what the client was expecting.

release/2.0.0-IntegratingRedux
---

  yarn add redux react-redux

release/2.1.0-IsomorphicRedux
---

release/2.2.0-DbAccessWithKnex
---

Install libs:

    yarn add pg knex@0.12.0 bcrypt-nodejs

note: We need to fix the dependency to `knex@0.12.0`, because there will be another library we'll be using later is not compatible with v0.13.0.

It is helpful to also install `knex` **globally**:

  yarn global add knex

Generate `knexfile.js`, run:

  knex init

, edit the knexfile.js and set-up PostgreSQL connection.

Run migration to add new table, supply migration name:

  knex migrate:make create_users

This generates new fule in the `./migrations` folder. Edit to create table.

Run the migration:

  knex migrate:latest

Next add some data with `seed`, create _admin_ migration for admin user:

  knex seed:make admin

, the file gets created as `./seeds/admin.js`

Edit the file, then run:

  knex seed:run


release/2.3.0-ORMWithBookshelf
---

Add the `faker` package:

  yarn add --dev faker

Create table `invoices`:

  knex migrate:make create_invoices
  knex migrate:latest

Insert sample data:

  knex seed:make invoices
  knex seed:run

Add connection class `app/server/connection.js`:

    import knex from 'knex'
    import config from '../../knexfile'
    
    export default knex(config)

Import in `server.js`. Add some debugs:

    import kx from './server/connection'
    ...
    kx.on('query', data => console.log(data))

    const invoices = kx.from('invoices')
        .select('total', 'email', 'username')
        .innerJoin('users', 'user.id', 'invoices.user_id')
        .then(rows => {
            debugger
            return rows
        })

    const users = kx.from('users')
        .select('username', 'total', 'email')
        .innerJoin('invoices', 'users.id', 'invoices.user_id')
        .then(rows => {
            debugger
            return rows
        })

Run the server in debug mode:

  yarn run server debug

  ... enter "c" to continue
  ... enter "repl" to show REPL
  ... console.dir(rows) - see rows variable
  ... Ctrl+C to exit

ORM - Install bookshelf:

  yarn add bookshelf

Change the file `./server/connection.js`:

Add models in `server/models`:

    -- Invoice.js
    import bookshelf from '../connection'

    export default bookshelf.Model.extend({
        tableName: 'invoices'
    })

    -- User.js
    import bookshelf from '../connection'
    import Invoice from './Invoice'

    export default bookshelf.Model.extend({
        tableName: 'users',
        invoices: function() {
            return this.hasMany(Invoice)
        }
    })

Now we can have eager loading. Edit `server.js`:

    import User from './server/models/User'
    import { kx } from './server/connection'
    ...
    const eagerUsers = User.fetchAll({
        withRelated: ['invoices']
    }).then(relation => {
        debugger
        return relation
    })

When we restart the server:

    yarn run server debug

Press "c", examine the SQL output. It first selects the users:

  select "users".* from "users"

, then the corresponding invoices:

  select "invoices".* from "invoices" where "invoices"."user_id" in (?)

, so it will merge the data together.

To view the results, type `repl`, then:

  console.dir(relation.toJSON())
  console.dir(relation.toJSON()[0])



release/3.0.0-TokenBasedAuth
---

Steps are:

- Create end point to submit user name and password and receive encrypted authentication token  
- Then create protected API.
  - It will raise error if it does not detect a valid token
- Along the way will be using a tool called `Insomnia` to test the API

To start we need to install new dependencies:

  yarn add body-parser jsonwebtoken

to parse the request body and to generate auth tokens.

Since we are doing alot of work we install `nodemon` to automatically restart the web server on changes in the code:

  yarn add --dev nodemon

Then change the server command in `package.json` from:

  "scripts": {
    "server": "yarn run build-client && node_modules/babel-cli/bin/babel-node.js app/server.js --presets es2015,react,stage-2",

to:

  "scripts": {
    "server": "yarn run build-client && nodemon app/server.js --exec node_modules/babel-cli/bin/babel-node.js --presets es2015,react,stage-2",

, then run the server:

  yarn run server


Edit `app/server.js`. Import the packages:

  import bodyParser from 'body-parser'
  import bcrypt from 'bcrypt-nodejs' 
  import jwt from 'jsonwebtoken'

, then tell the express to use the bodyParser middleware:

  const app = express()
  app.use(bodyParser.json())


release/3.1.0-CookiesAndCSRF
---

Using cookie to store the auth-token makes it easier for the client to supply the token.

It opens a possibility for _Cross Site Forgery Vulnerability_.

Use _http-only plus CSFR tokens_ for best security and to keep from _Cross Site Scripting_ attack.

Http only cookie: cannot be readed by javascript on the cliekt.

Add cookie parser:

    yarn add cookie-parser csrf
 

CSRF cookie:

- Token value changes with each request.  
- We cannot use the http-only option, because we need the client to be able to read it.
- We only write it to authenticated requests.


To communicate from the client to the server, as well as CORS:

  yarn add axios cors

Use axios in the client.js.
Use cors on the server.js.


release/3.2.0-LoginForm
---


**Split reducers**

Split the reducers. Copy the `reducer.js` into `rgbReducer.js`. The `reducer.js` will import the `combineReducers` function from redux.

Then the users of the original reducer need to change - see `RGB.jsx`. We cannot anymore import RGB from the state object. We need to access `state.rgb`. Then this:

  export default connect(
    state => ({
      r: state.r,

changes to this:

  export default connect(
    state => state.rgb,
    { updateColor: actions.updateColor }
  )(RGB)

as now the `state.rgb` actually already is exactly the object that we need.


**Session container**

The session related go into _session_ folder.

add `api.js` file that has all the api related work. move out from `client.jsx`.

This is how the `api.js` looks like:

  import axios from 'axios'

  const adapter = axios.create({
    baseURL: 'http://localhost:9999/api',
    withCredentials: true,
    headers: {
      Accept: 'application/json'
    }
  })

  export default {
      session: {
          login: (username, password) => adapter.post('/session', { username, password })
      }
  }

Now we can write an action creator that authenticates with the API.
We will not be able to use with the API, because redux by default expects actions to be plain objects.

In order to be able to make actions that use async API calls, we need async middleware. We install:

   yarn add redux-thunk

Then we update `client.jsx` to use the middleware. We import `applyMiddleware` from redux and the `thunk` middleware:

  import { createStore, applyMiddleware } from 'redux'
  import thunk from 'redux-thunk'

and pass int to `createStore()`

Then write the actions in `session/actions.js`. Define acion creator `login()`, that accepts username and password.

To make it work with redux-thunk middlewarw, we return a function, that accepts a `dispatch` as first argument.



release/3.3.0-StayLogged
---

Move all the the store create logic into a function that both the client and the server use  `app/universal/createStore.js`.

Modify `app/universal/api.js` to persist the user.


Check for persistet user when the page loads in `client.jsx`. We replace the old createStore logic and start using the new `createStore.js`. Then check the local storage.


Then change the server side - the middleware. When running server side rener mode, 
we want every request to have access to the current user.

In `server.js` instead of raise error when not authenticated, we set flags. See `decode()`. Make sure we always call `next()` instead of returning errors. We add `verify()` that is to raise errors for the API.
We use the `decode` middleware on every request and 

Next update the server side rendering `serverRender.jsx`. Again - we remove the store creation logic and reuse `createStore`.
Also - we set the curre t user in the redux store it it exist. The cookie decode middleware runs just before this function.

What should hapoen when the auth token expires? The api client should check to 403 when the session expires.
Added call to GET session in the `api.js`. Added `info()` and call every time the app starts.

If we change the token creation in the `server.js` and make 1min token, then after 1min refresh the page we will see in the debugger window 403 error.

We create axios interseptor - behaviour to run when request fails. In our case we will remove the user from the localStore and the redux store.

We start with adding session action CLEAR  ... TODO ... 
 

release/4.0.0-ReactRouter
---

Adding:
- Client side URL to view mapping
- Server side rendering mapping to the URL
- Full cycle of the authentication

Plan:
- Replace the manual conditional display logic with route based
- Add routes and redirects
- Add additional routes to navigation mennu

Add:

  yarn add react-router react-router-dom

Declare the routes in a top-level component:

  app/universal/App.jsx

Change the app/client.jsx. Use App instead of Home component. It also uses BrowserRouter.

Changes to the components that render the Home and the login. Home.jsx does not need to render the Login.

Basically instead of rendering components conditionally, we render redirects.

Home.jsx code:

    export function Home({ authenticated }) {
        if (!authenticated) {
            return <Redirect to={{ pathname: './login' }} />
        } 
        
        return (
            <div>
                <Route exact path ="/" component={RGB} />
            </div>
        )
    }

Note the `exact` property that forces exact matching.
It won't render if the path contains any path fragments after the slash.

Login.jsx - it maintains state `success` variable to make redirect if it is `true`.


Navigation menu

Define quick function to conditionally apply class names in `app/lib/clasNames.js`.

Define component `app/universal/NavLink.js' for the indivisual links.

Define menu to render the menu links `app/universal/Nav.jsx`.

The update the Home component to display the links.




release/4.1.0-IsomorphicRouting
---

Will:

- add server side log with `morgan` middleware  
- use the static router on server side rendering  
- use static context to return 301 and 404 status codes where appropriate

Add morgan:

  yarn add morgan

Include in `server.js`.
Hookup static router into `serverRender.jsx`. Note: we are using StaticRouter component:

        <Provider store={store}>
            <StaticRouter location={req.url}>
                <App /> 
            </StaticRouter>
        </Provider>

Then add server side redirect 301 to the login url.
We add empty context object and send to the StaticRouter component.
If it was populated with url property, we redirect to that url.


Add 404 page. 

Edit Home.jsx, wrap the routes in a `Switch`. Set the `staticContext.status` to 404, not found.
The `staticContext` if only available on serverside rendering.

Code:

   return (
        <div>
            <Nav />
            <Switch>
                <Route exact path ="/" component={RGB} />
                <Route children={ ({ staticContext }) =>   {
                    if (staticContext) {
                        staticContext.status = 404
                    }
                    return <h1>Sorry, can't find that!</h1>
                }}/>
            </Switch>
        </div>
    )


Then in `serverRender.jsx` use the `status`:

    if (context.url) {
        res.redirect(301, context.url)
    } else {
        const status = context.status || 200
        res.status(status).send(template(html, store.getState()))
    }

Now if we restart the server, go to http://localhost:9999, login, then try to hit http://localhost:9999/foobar, we should see file not found page.


release/4.2.0-LoggingOut
---

release/5.0.0-CRUD
---

release/5.1.0-FormWithValidation
---

release/5.2.0-BackendValidations
---

release/6.0.0-ReduxDataTables
---

release/6.1.0-ReduxPagination
---

release/7.0.0-ReusingActionsAndReducers
---

release/8.1.0-PreloadDataWithSSR
---

release/8.2.0-TestingRedux
---

Using `mocha` as test runner and expect for the asserts. installing `babel-register` compiler in order for mocha to recognize the syntax:

  yarn add --dev mocha expect babel-register

*First* we create tests for the pagination reducer in `shared/pagination/reducer.spec.js`.

We need to export the `initialState` object, so we can access it from the tests: `export const initialState = { ...`.

*Second* we create tests for the customer actions in `app/universal/customers/actions.spec.js`.

To mock asynchronous calls, ass `axios-mock-adapter`, and `redux-mock-store` will be needed to simulate dispatching actions:

  yarn add --dev axios-mock-adapter redux-mock-store

We need to export the adapter from the `app/universal/app.js`: `export const adapter = axios.create({...`.

Outside of this course is _testing view components_.

React's virtual DOM makes testing view output and user interactions easier than ever.

`Enzyme` and `Jest` both offer fully features test renderers, event simulation, and more.

...

Or using `jest` https://github.com/facebook/jest ????:

  yarn add --dev jest
  yarn add --dev jest-cli
  yarn add --dev babel-jest babel-core regenerator-runtime

release/8.3.0-TypeChecking

One popular way to add type checking is using the popular `prop-types` library:

  yarn add prop-types

`PropTypes` adds _runtime type checking_.

This will show warnings on the browser's console if type mismatched.
The props type checking is stripped out in production, so it only adds during development.

See: app/universal/rgb/Slider.jsx

Another way to add type checking is using the `flow js`:

  yarn add --dev flow-bin
  yarn global add flow-bin

`Flow compiler` adds _static type checking_

You can add one file at a time. It builds graph of types, but marks as unknown where not type not annotated.

We need to generate flow config file:

  yarn run flow init

Edit the `.flowconfig` file, add ignores and libs

  [ignore]
  ./assets
  ./migrations
  ./node_modules
  ./seeds
  .*package\.json

Add to the top of the Slider.jsx:

  // @flow

, annotation, that this file should be checked.

Now, if we run the flow checker:

  yarn run flow

it will raise an error, that we are missing type annotations:

  Error: app/universal/rgb/Slider.jsx:6
    6: export default function Slider ({ value, slide }) {
                                      ^^^^^^^^^^^^^^^^ destructuring. Missing annotation

Install `Flow Language Support` extension for the visual studio code.
Also, for `eslint` we need [eslint-plugin-flowtype](https://github.com/gajus/eslint-plugin-flowtype):

  yarn add --dev eslint
  yarn add --dev babel-eslint
  yarn add --dev eslint-plugin-flowtype

Change the `.eslintrc.json` to support the `Flow`:

  {
      "extends": ["standard", "standard-jsx", "plugin:react/recommended", "plugin:flowtype/recommended"],
      "plugins" : [
          "react",
          "flowtype"
      ]
  }

Then change the VS Code user settings:

  {
      "files.associations": {
          "*.js": "flow"
      },
      "javascript.validate.enable": false,
      "flow.useNPMPackagedFlow": true
  }

You may add in `package.json` to the scripts:

  "flow": "./node_modules/.bin/flow"

Note: Place those settings provided by @agrcrobles in $workspace/.vscode/settings.json will only effective in current project.

See [eslint-plugin-flowtype-installation](https://github.com/gajus/eslint-plugin-flowtype#eslint-plugin-flowtype-installation)
