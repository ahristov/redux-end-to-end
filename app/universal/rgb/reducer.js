// Reducer: Manage the state.

import * as actionTypes from './actionTypes'
import reduce from '../reduce'

const initialState = {
  r: 55,
  g: 255,
  b: 55
}

function updateColor (state, action) {
  return {
    ...state,
    [action.color]: action.value
  }
}

export default reduce(initialState, {
  [actionTypes.SLIDE]: updateColor
})
