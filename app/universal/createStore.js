import { compose, createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import reducer from './reducer'

let store = null

export default function create (server = false) {
  const composeEnhancers = server
    ? compose
    : ((typeof window !== 'undefined') ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : undefined) || compose

  if (!store) {
    store = createStore(
      reducer,
      server ? undefined : ((typeof window !== 'undefined') ? window.__PRELOADED_STATE__ : undefined) || undefined,
      /* window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(), */
      composeEnhancers(
        applyMiddleware(thunk)
        // , .... another middleware here
      )
    )
  }

  return store
}
