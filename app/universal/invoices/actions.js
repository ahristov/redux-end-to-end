import { SubmissionError } from 'redux-form'
import api from '../api'
import actions from '../shared/pagination/actions'

export const { resultsUpdated } = actions('invoices')

export function create (data) {
  return () => api.invoices.create(data)
    .then(() => 'SUCCESS') // the bellow catch will swallow the response and prevent the redirect in the form, thereof this then().
    .catch((err) => {
      if (err.response.status === 422) {
        throw new SubmissionError(err.response.data.errors)
      }

      throw err
    })
}

export function list (page = 1, pageSize = 15) {
  return dispatch => api.invoices.list(page, pageSize).then(resp =>
    dispatch(resultsUpdated(resp.data))
  )
}
