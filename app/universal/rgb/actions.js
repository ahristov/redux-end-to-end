// Action: Send message to the reducer.
// Action creators: Defines reusable functions called action creators that return redux actions.

import * as actionTypes from './actionTypes'

export function updateColor (color, value) {
  return {
    type: actionTypes.SLIDE,
    color,
    value
  }
}
