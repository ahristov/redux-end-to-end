import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import queryString from 'query-string'
import actions from './actions'

/**
 * Will be used every time we want to do a data table.
 * We separate logic from presentation: "smart: and "dumb" components.
 * This component contains the pagination logic.
 *
 * @export
 * @param {any} name Specifies the reducer where we store the results.
 * @param {any} fetch Redux action to retrieve the results.
 * @returns
 */
export default function tabulate (name, fetch) {
  return Table => {
    /**
         * Return a function that takes the component to be decorated as the only argument
         * and returns a `Component` class called `Tabulated`.
         *
         * Inherit from the higher order `Component` function, sometimes also known as a `decorator`.
         *
         * @class Tabulated
         * @extends {Component}
         */
    class Tabulated extends Component {
      componentWillMount () {
        const { stale, setPage, page, pageSize, location } = this.props

        const qs = queryString.parse(location.search)

        if (stale) {
          setPage(parseInt(qs.page) || page, parseInt(qs.pageSize) || pageSize)
        }
      }

      componentDidMount () {
        const { stale, fetch } = this.props

        if (stale) {
          fetch()
        }
      }

      componentWillReceiveProps (nextProps) {
        const { stale, fetch } = nextProps

        if (stale) {
          fetch()
        }
      }

      render () {
        return <Table results={this.props.results} />
      }
    }

    // Inject all the properties from the named reducer.
    // As second argument we inject the provided `fetch` function.

    // return connect(state => state[name], { fetch })(Tabulated)

    const { setPage } = actions(name)

    return connect(state => state[name])(connect(
      undefined,
      (dispatch, ownProps) => ({
        fetch: () => dispatch(fetch(ownProps.page, ownProps.pageSize)),
        setPage: (page, pageSize) => dispatch(setPage(page, pageSize))
      })
    )(withRouter(Tabulated)))
  }
}
