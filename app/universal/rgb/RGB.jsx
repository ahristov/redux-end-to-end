// State container: holds the data.

import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as actions from './actions'
import ColorBlock from './ColorBlock'
import Slider from './Slider'

export class RGB extends Component {
  render () {
    const { r, g, b, updateColor } = this.props // destructure state variables.
    const update = color => val => updateColor(color, val)

    return (
      <center>
        <ColorBlock r={r} g={g} b={b} />
        <Slider value={r} slide={update('r')} />
        <Slider value={g} slide={update('g')} />
        <Slider value={b} slide={update('b')} />
      </center>
    )
  }
}

// Decorate this component with `connect` from `react-redux`.
// Thereof we do not export the component but the decorated with `connect` component.
//
// Arguments:
// - (1) Function that takes the application state and returns an object represents the properties we want.
// - (2) Object that contains acton creators. WIll bind all the calls to these with calls to the dispatch, which is how it gets send to the Redux store.
//
export default connect(
  state => state.rgb,
  { updateColor: actions.updateColor }
)(RGB)
