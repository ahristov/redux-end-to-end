// Knex only

/*
import knex from 'knex'
import config from '../../knexfile'

export default knex(config)
*/

// Knex with Bookshelf

import knex from 'knex'
import bookshelf from 'bookshelf'
import config from '../../knexfile'

export const kx = knex(config)

const orm = bookshelf(kx)
orm.plugin('pagination')

export default orm
