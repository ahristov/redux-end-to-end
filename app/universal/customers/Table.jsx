import React from 'react'
import * as actions from './actions'
import { tabulate } from '../shared/pagination'

// Does not need tom import `react-redux` or the `Component` module,
// as we now have a pure functional component that uses the decorator `tabulate`.
// Thereof we remove the life cycle logic below and define pure function.

export function Table ({ results }) {
  return (
    <table className='pure-table'>
      <thead>
        <tr>
          <th>Email Address</th>
        </tr>
      </thead>
      <tbody>
        {results.map(r => (
          <tr key={r.id}>
            <td>{r.email}</td>
          </tr>
        ))}
      </tbody>
    </table>
  )
}

// Apply the default decorator to our component.
// Use the `customers` reducer, and the `list` action to fetch data.

export default tabulate('customers', actions.list)(Table)
