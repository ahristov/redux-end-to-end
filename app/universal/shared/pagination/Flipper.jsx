import React from 'react'
import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import actions from './actions'

export function Flipper ({ previous, next, page, pageSize, totalCount }) {
  const totalPages = Math.ceil(totalCount / pageSize)
  const hasPreviousPage = page > 1
  const hasNextPage = page < totalPages

  return (
    <div className='pure-button-group'>
      <button className='pure-button' disabled={!hasPreviousPage} onClick={previous}>
                &lt;
      </button>
      <button className='pure-button' disabled={!hasNextPage} onClick={next}>
                &gt;
      </button>
    </div>
  )
}

Flipper.propTypes = {
  previous: PropTypes.func,
  next: PropTypes.func,
  page: PropTypes.number,
  pageSize: PropTypes.number,
  totalCount: PropTypes.number
}
export default connect(
  (state, ownProps) => state[ownProps.listId],
  (dispatch, ownProps) => bindActionCreators(actions(ownProps.listId), dispatch)
)(Flipper)
