/* eslint-env mocha */
import expect from 'expect'
import thunk from 'redux-thunk'
import configureStore from 'redux-mock-store'

import { SubmissionError } from 'redux-form'
import MockAdapter from 'axios-mock-adapter'
import { adapter } from '../api'
import * as actions from './actions'

const mockStore = configureStore([thunk])
const mockAdapter = new MockAdapter(adapter)

describe('customer actions', () => {
  // Always good to reset the adapter mock after each test.
  afterEach(() => {
    mockAdapter.reset()
  })

  describe('list()', () => {
    const data = { totalCount: 0, results: [] }

    beforeEach(() => {
      mockAdapter.onGet('/customers').reply(200, data)
    })

    it('dispatches resultsUpdated', () => {
      const store = mockStore()

      // When we get successful response,
      // the `resultsUpdated()` action will get called with the expected arguments.
      return store.dispatch(actions.list()).then(() => {
        expect(store.getActions()).toContain(actions.resultsUpdated(data))
      })
    })
  })

  describe('create()', () => {
    context('on validation failure', () => {
      beforeEach(() => {
        mockAdapter.onPost('/customers').reply(422, { errors: {} })
      })

      it('throws a SubmissionError', () => {
        const store = mockStore()

        // We call `catch()`, since we expect error to be thrown.
        return store.dispatch(actions.create({})).catch((err) => {
          expect(err).toBeA(SubmissionError)
        })
      })
    })
  })
})
