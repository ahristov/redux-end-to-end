// Reducer: Manage the state.

import { combineReducers } from 'redux'
import { reducer as form } from 'redux-form'
import rgb from './rgb/reducer'
import session from './session/reducer'
import paginate from './shared/pagination/reducer'

export default combineReducers({
  form,
  rgb,
  session,
  customers: paginate('customers'),
  invoices: paginate('invoices')
})
