import React from 'react'
import { render } from 'react-dom'
import createStore from './universal/createStore'
import { Provider } from 'react-redux'
import { BrowserRouter } from 'react-router-dom'
import App from './universal/App'
import { setUser } from './universal/session/actions'
import './app.scss'

const store = createStore()

// api.session.info()

const serializedUser = window.localStorage.getItem('user')
if (serializedUser) {
  store.dispatch(setUser(JSON.parse(serializedUser)))
}

// Store exposes:
// - dispatch: Takes redux action. Allows to send actions to the store.
// - getState(): Returns the entire application state from the reducers.
// - subscribe(): Allows to listen for changes to the redux store.

// Provider: We need to wrap the whole app in the `Provider` component.
// This provides the context to wrap all the children to the Redux store.
render(
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>,
  document.getElementById('app')
)
