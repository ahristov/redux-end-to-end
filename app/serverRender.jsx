import React from 'react'
import { StaticRouter, matchPath } from 'react-router'
import { renderToString } from 'react-dom/server'
import createStore from './universal/createStore'
import { Provider } from 'react-redux'
import template from './template'
import App from './universal/App'
import { setUser } from './universal/session/actions'
import { updateColor } from './universal/rgb/actions'

import actions from './universal/shared/pagination/actions'
import * as customersService from './server/services/customers'
import * as invoicesService from './server/services/invoices'

function preloadedRoutes (store, req) {
  const loadPage = (listId, service) => {
    const { resultsUpdated, setPage } = actions(listId)

    const {
      page = 1,
      pageSize = store.getState()[listId].pageSize
    } = req.query

    store.dispatch(setPage(page, pageSize))

    return service.list({ page, pageSize }, req.currentUser).then(page => {
      store.dispatch(resultsUpdated(page))
    })
  }

  return [{
    path: '/customers',
    load: () => loadPage('customers', customersService)
  }, {
    path: '/invoices',
    load: () => loadPage('invoices', invoicesService)
  }]
}

function loader (req, store) {
  const route = preloadedRoutes(store, req).find(r => matchPath(req.path, r))

  if (!route || !req.currentUser) {
    return () => Promise.resolve()
  }

  return route.load
}

export default function render (req, res) {
  const store = createStore(true)

  // Modify the store prior the server side render.
  store.dispatch(updateColor('r', 255))
  store.dispatch(updateColor('g', 55))
  store.dispatch(updateColor('b', 55))

  if (req.currentUser) {
    store.dispatch(setUser(req.currentUser))
  }

  const context = {}

  const dataLoader = loader(req, store)

  dataLoader().then(() => {
    const html = renderToString(
      <Provider store={store}>
        <StaticRouter location={req.url} context={context}>
          <App />
        </StaticRouter>
      </Provider>
    )

    if (context.url) {
      res.redirect(301, context.url)
    } else {
      const status = context.status || 200
      res.status(status).send(template(html, store.getState()))
    }
  })

  // Now that the server side template will be pre-rendered with modified RGB values,
  // different than the default store values, thereof different that on the client render.
  //
  // This will result of checksum warning on the React client side rendering.
  //
  // To have the both worlds share same initial data,
  // we need to dump the state of the server side store.
  //
  //      res.send(template(html))
  // , so we send the `state.getState()`.
  // Check the `template.js` file how the store gets send to the UI via a global variable.
  // Check the `client.jsx` file how the initial state is set from the global variable.
}
